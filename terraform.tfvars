# root/terraform.tfvars

# define aws region
my_region = "us-east-1"

# define vpc
vpc_cidr = "10.10.0.0/16"
vpc_name = "my_vpc"
vpc_subnets = [{
  name              = "us-east-1a"
  cidr_block        = "10.10.0.0/24"
  availability_zone = "us-east-1a"
  }, {
  name              = "us-east-1b"
  cidr_block        = "10.10.1.0/24"
  availability_zone = "us-east-1b"
  }
]

# define security group
security_groups = [{
  name        = "web-sg"
  description = "allow http only"
  ingress = {
    from_port   = 0
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress = {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  }, {
  name        = "ssh-sg"
  description = "allow ssh only"
  ingress = {
    from_port   = 0
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress = {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}]


# define ec2 instances
ec2_instances = [{
  name          = "web-1a"
  instance_type = "t2.micro"
  ami           = "ami-06e46074ae430fba6"
  subnet_name   = "us-east-1a"
  sg_list       = ["web-sg", "ssh-sg"]
  user_data     = "user_data.sh"
  }, {
  name          = "web-1b"
  instance_type = "t2.micro"
  ami           = "ami-06e46074ae430fba6"
  subnet_name   = "us-east-1b"
  sg_list       = ["web-sg"]
  user_data     = "user_data.sh"
  },
  # {
  # name          = "web-2a"
  # instance_type = "t2.micro"
  # ami           = "ami-06e46074ae430fba6"
  # subnet_name   = "us-east-1a"
  # sg_list       = ["web-sg"]
  # user_data     = "user_data.sh"
  # }
]

# define an elb

elb_name    = "web-alb"
elb_sg_list = ["web-sg"]
tg_name     = "web"

# define domain name
domain_name     = "jarunranrun.me"
sub_domain_name = "my-assignment"