# compute/main.tf

# create new EC2 instances
resource "aws_instance" "node" {
  for_each = {
    for instance in var.ec2_instances :
    instance.name => instance
  }

  instance_type = each.value.instance_type
  ami           = each.value.ami
  subnet_id     = var.subnets[each.value.subnet_name].id
  
  vpc_security_group_ids = compact([for vpc_security_group in var.vpc_security_groups :
    contains(each.value.sg_list, vpc_security_group.name) ? vpc_security_group.id : ""
  ])

  user_data = file(each.value.user_data)

  tags = {
    Name = each.key
  }
}
