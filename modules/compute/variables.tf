# # compute/variable.tf
variable "subnets" {
  type    = map(any)
  default = {}
}

variable "ec2_instances" {
  type = list(object({
    name          = string
    instance_type = string
    ami           = string
    subnet_name   = string
    sg_list       = list(string)
    user_data     = string
    })
  )
}

variable "vpc_security_groups" {
  type    = map(any)
  default = {}
}