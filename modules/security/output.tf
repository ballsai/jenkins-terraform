# security/output.tf

# return security groups
output "vpc_security_groups" {
  value = aws_security_group.sg
}