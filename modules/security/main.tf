# security/main.tf

# create new Security Group
resource "aws_security_group" "sg" {

  for_each = {
    for security_group in var.security_groups :
    security_group.name => security_group
  }

  vpc_id      = var.vpc_id
  name        = each.key
  description = each.value.description

  ingress {
    from_port   = each.value.ingress.from_port
    to_port     = each.value.ingress.to_port
    protocol    = each.value.ingress.protocol
    cidr_blocks = each.value.ingress.cidr_blocks
  }

  egress {
    from_port   = each.value.egress.from_port
    to_port     = each.value.egress.to_port
    protocol    = each.value.egress.protocol
    cidr_blocks = each.value.egress.cidr_blocks
  }

  tags = {
    Name = each.key
  }
}