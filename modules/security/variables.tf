# security/variables.tf

variable "vpc_id" {
  type = string
}

variable "security_groups" {
  type = list(any)
  default = [{
    name        = "default-sg"
    description = "allow  all"

    ingress = {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }
    egress = {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }]
}