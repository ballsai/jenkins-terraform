# dns/variables.tf

variable "domain_name" {
  type    = string
  default = "example.com"
}

variable "sub_domain_name" {
  type    = string
  default = ""
}

variable "cdn_domain_name" {
  type    = string
  default = ""
}

variable "cdn_hosted_zone_id" {
  type    = string
  default = ""
}
