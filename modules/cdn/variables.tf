# cdn/varibles.

variable "alb_dns_name" {
  default = ""
}

variable "domain_name" {
  type    = string
  default = "example.com"
}

variable "sub_domain_name" {
  type    = string
  default = ""
}

variable "acm_certificate_arn" {
  type    = string
  default = ""
}