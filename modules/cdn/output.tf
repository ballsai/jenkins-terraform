# cnd/output.tf

output "cdn_domain_name" {
  value = aws_cloudfront_distribution.alb_distribution.domain_name
}

output "cdn_hosted_zone_id" {
  value = aws_cloudfront_distribution.alb_distribution.hosted_zone_id
}