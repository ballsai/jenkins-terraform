# network/variables.tf

variable "vpc_cidr" {
  type        = string
  default     = "10.0.0.0/16"
  description = "define vpc cidr"
}

variable "vpc_name" {
  type = string
}

variable "vpc_subnets" {
  type = list(object({
    name              = string
    cidr_block        = string
    availability_zone = string
  }))
  default = []
}

variable "igw_name" {
  type    = string
  default = "my-igw"
}

variable "main_RT_name" {
  type    = string
  default = "main_RT"
}