# network/main.tf

# create a new vpc
resource "aws_vpc" "vpc" {
  cidr_block = var.vpc_cidr
  tags = {
    Name = var.vpc_name
  }
}

# create new subnets
resource "aws_subnet" "subnets" {
  for_each = {
    # convert list to map
    for subnet in var.vpc_subnets :
    subnet.name => subnet
  }

  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = each.value.cidr_block
  availability_zone       = each.value.availability_zone
  map_public_ip_on_launch = true

  tags = {
    Name = each.value.name
  }
}

# create a new internet gateway
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id
  tags = {
    Name = var.igw_name
  }
}

# determine a main RT's name
resource "aws_default_route_table" "main_RT" {
  default_route_table_id = aws_vpc.vpc.default_route_table_id
  tags = {
    Name = var.main_RT_name
  }
}

# determine a default route to Internet Gateway on main RT
resource "aws_route" "default_route" {
  route_table_id         = aws_vpc.vpc.default_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.igw.id
}

# public subnets associate with main RT
resource "aws_route_table_association" "public_subnet_association" {
  for_each       = aws_subnet.subnets
  subnet_id      = each.value.id
  route_table_id = aws_vpc.vpc.default_route_table_id
}