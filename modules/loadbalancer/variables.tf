# loadbalancer/variables.tf

variable "elb_name" {
  type    = string
  default = ""
}

variable "elb_sg_list" {
  type    = list(any)
  default = []
}

variable "tg_name" {
  type = string
}

variable "health_check" {
  type = map(string)
  default = {
    "timeout"             = "10"
    "interval"            = "20"
    "path"                = "/"
    "port"                = "80"
    "unhealthy_threshold" = "2"
    "healthy_threshold"   = "3"
  }
}


variable "subnets" {
  type    = map(any)
  default = {}
}

variable "vpc_id" {
  type    = string
  default = ""
}

variable "vpc_security_groups" {
  type    = map(any)
  default = {}
}

variable "target_id" {
  type    = string
  default = ""
}

variable "instances" {
  type    = map(any)
  default = {}
}