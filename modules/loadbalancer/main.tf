# loadbalancer/main.tf

# create application load balancer  
resource "aws_lb" "alb" {
  name               = var.elb_name
  internal           = false
  load_balancer_type = "application"
  security_groups = compact([for vpc_security_group in var.vpc_security_groups :
    contains(var.elb_sg_list, vpc_security_group.name) ? vpc_security_group.id : ""
  ])
  subnets                          = [for subnet in var.subnets : subnet.id]
  enable_cross_zone_load_balancing = "true"
  tags = {
    Enviroment = "dev"
  }
}

# create target group 
resource "aws_lb_target_group" "tg" {
  name        = var.tg_name
  target_type = "instance"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = var.vpc_id
  health_check {
    healthy_threshold   = var.health_check["healthy_threshold"]
    interval            = var.health_check["interval"]
    unhealthy_threshold = var.health_check["unhealthy_threshold"]
    timeout             = var.health_check["timeout"]
    path                = var.health_check["path"]
    port                = var.health_check["port"]
  }
}

# attach EC2 instances to  target group
resource "aws_lb_target_group_attachment" "tg_attachment" {
  target_group_arn = aws_lb_target_group.tg.arn
  for_each         = var.instances
  target_id        = each.value.id
  port             = 80
}

# add listener rule for HTTP on ALB
resource "aws_lb_listener" "alb_listener_http" {
  load_balancer_arn = aws_lb.alb.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    target_group_arn = aws_lb_target_group.tg.arn
    type             = "forward"
  }
}
