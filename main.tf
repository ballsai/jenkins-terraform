# root/main.tf

# create a new vpc
module "network" {
  source       = "./modules/network"
  vpc_cidr     = var.vpc_cidr
  vpc_name     = var.vpc_name
  vpc_subnets  = var.vpc_subnets
  igw_name     = var.igw_name
  main_RT_name = var.main_RT_name
}

# create new security group
module "security" {
  source          = "./modules/security"
  security_groups = var.security_groups
  vpc_id          = module.network.vpc_id
}

# create ec2 instances
module "compute" {
  source              = "./modules/compute"
  ec2_instances       = var.ec2_instances
  vpc_security_groups = module.security.vpc_security_groups
  subnets             = module.network.subnets
}

# create a load balancer and a target group
module "loadbalancer" {
  source              = "./modules/loadbalancer"
  elb_name            = var.elb_name
  elb_sg_list         = var.elb_sg_list
  tg_name             = var.tg_name
  vpc_security_groups = module.security.vpc_security_groups
  subnets             = module.network.subnets
  vpc_id              = module.network.vpc_id
  instances           = module.compute.instances
}

# distribute load balancer to cloudfront
module "cdn" {
  source              = "./modules/cdn"
  alb_dns_name        = module.loadbalancer.alb_dns_name
  domain_name         = var.domain_name
  sub_domain_name     = var.sub_domain_name
  acm_certificate_arn = module.dns.acm_certificate_arn
}

# forward a domain name to cloudfront  
module "dns" {
  source             = "./modules/dns"
  cdn_domain_name    = module.cdn.cdn_domain_name
  cdn_hosted_zone_id = module.cdn.cdn_hosted_zone_id
  domain_name        = var.domain_name
  sub_domain_name    = var.sub_domain_name
}