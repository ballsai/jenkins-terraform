# root/variables.tf

variable "my_region" {
  type        = string
  default     = "us-east-1"
  description = "define region"
}

variable "vpc_cidr" {
  type = string
}

variable "vpc_name" {
  type = string
}

variable "vpc_subnets" {
  type = list(object({
    name              = string
    cidr_block        = string
    availability_zone = string
  }))
  default = []
}

variable "igw_name" {
  type    = string
  default = "my-igw"
}

variable "main_RT_name" {
  type    = string
  default = "main_RT"
}

variable "vpc_id" {
  type    = string
  default = ""
}

variable "subnet_id" {
  type    = string
  default = ""
}

variable "security_groups" {
  type = list(any)
  default = [{
    name        = "default-sg"
    description = "allow  all"

    ingress = {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }
    egress = {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }]
}

variable "vpc_security_groups" {
  type    = map(any)
  default = {}
}

variable "ec2_instances" {
  type = list(object({
    name          = string
    instance_type = string
    ami           = string
    subnet_name   = string
    sg_list       = list(string)
    user_data     = string
    })
  )
}

variable "elb_name" {
  type    = string
  default = ""
}

variable "elb_sg_list" {
  type    = list(any)
  default = []
}

variable "tg_name" {
  type = string
}

variable "health_check" {
  type = map(string)
  default = {
    "timeout"             = "10"
    "interval"            = "20"
    "path"                = "/"
    "port"                = "80"
    "unhealthy_threshold" = "2"
    "healthy_threshold"   = "3"
  }
}

variable "subnets" {
  type    = map(any)
  default = {}
}

variable "target_id" {
  type    = string
  default = ""
}

variable "alb_dns_name" {
  default = ""
}

variable "instances" {
  type    = map(any)
  default = {}
}

variable "cdn_domain_name" {
  type    = string
  default = ""
}

variable "cdn_hosted_zone_id" {
  type    = string
  default = ""
}

variable "domain_name" {
  type    = string
  default = "example.com"
}

variable "sub_domain_name" {
  type    = string
  default = ""
}

variable "acm_certificate_arn" {
  type    = string
  default = ""
}

